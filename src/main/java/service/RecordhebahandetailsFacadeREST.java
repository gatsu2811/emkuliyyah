/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Recordhebahandetails;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 *
 * @author emafazillah
 */
@Stateless
@Path("/recordhebahandetails")
public class RecordhebahandetailsFacadeREST extends AbstractFacade<Recordhebahandetails> {

    @PersistenceContext(unitName = "emkuliyyah_emkuliyyah_war_1.0PU")
    private EntityManager em;

    public RecordhebahandetailsFacadeREST() {
        super(Recordhebahandetails.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_JSON})
    public void create(Recordhebahandetails entity) {
        super.create(entity);
    }

    @PUT
    @Path("/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Recordhebahandetails entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("/{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Recordhebahandetails find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_JSON})
    public List<Recordhebahandetails> findAll() {
        return super.findAll();
    }

    @GET
    @Path("/{from}/{to}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Recordhebahandetails> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("/count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    // Truncate table
    @DELETE
    @Path("/truncatetable")
    public void cleartable() throws IOException {
        //Clear table
        List<Recordhebahandetails> lrdc = super.findAll();
        for(Recordhebahandetails rdc : lrdc){
            super.remove(rdc);
        }                
    }
    
    //Data scraping from IKIM FM
    @POST
    @Path("/dsikim")
    public void dsikim() throws IOException {
        Document doc = Jsoup.connect("http://ikimfm.my/hebahanv2/index.php?").get();
        //paging
        Elements pages = doc.select("div.pagination a");
        for (Element page : pages) {
            for (Node child : page.childNodes()) {
                if (child instanceof TextNode) {                    
                    //start scraping
                    List<Recordhebahandetails> lrd = new ArrayList<>();                    
                    String pageno = ((TextNode) child).text();
                    String pageurl = "http://ikimfm.my/hebahanv2/index.php?page=" + pageno;
                    Document docpage = Jsoup.connect(pageurl).get();                    
                    //get all links and recursively call the processPage method
                    Elements questions = docpage.select("a[href]");
                    for (Element link : questions) {
                        if (link.attr("href").contains("hebahan-view.php?hebahanID=")) {
                            Recordhebahandetails rd = new Recordhebahandetails();
                            rd.setUrl(link.attr("abs:href"));
                            rd.setTitle(link.select("strong").get(0).toString());
                            lrd.add(rd);
                        }
                    }
                    int row;
                    row = 0;
                    Elements eanjurans = docpage.select("div.panel-body strong");
                    for (Element eanjuran : eanjurans) {
                        if (row < lrd.size()) {
                            for (Node childpage : eanjuran.childNodes()) {
                                if (childpage instanceof TextNode) {
                                    Recordhebahandetails rd = new Recordhebahandetails();
                                    String field = ((TextNode) childpage).text();
                                    switch (field) {
                                        case "Anjuran : ":
                                            rd.setAnjuran(eanjuran.nextSibling().toString());
                                            lrd.get(row).setAnjuran(rd.getAnjuran());
                                            break;
                                        case "Tarikh : ":
                                            rd.setTarikh(eanjuran.nextSibling().toString());
                                            lrd.get(row).setTarikh(rd.getTarikh());
                                            break;
                                        case "Lokasi : ":
                                            rd.setLokasi(eanjuran.nextSibling().toString());
                                            lrd.get(row).setLokasi(rd.getLokasi());
                                            row++;
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    //details
                    row = 0;
                    Elements edetails = docpage.select("p.bg-info");
                    for (Element edetail : edetails) {
                        if (row < lrd.size()) {
                            for (Node childpage : edetail.childNodes()) {
                                if (childpage instanceof TextNode) {
                                    Recordhebahandetails rd = new Recordhebahandetails();
                                    rd.setDetails(((TextNode) childpage).text());
                                    lrd.get(row).setDetails(rd.getDetails());
                                    row++;
                                }
                            }
                        }
                    }                    
                    //check existing and insert into db
                    List<Recordhebahandetails> lrdex = super.findAll();
                    if (lrdex.size() > 0) {
                        for (Recordhebahandetails rd : lrd) {
                            int counter = 0;
                            
                            for (Recordhebahandetails rdex : lrdex) {
                                if(rd.getUrl().contains(rdex.getUrl())){
                                    counter++;
                                }
                            }
                            if(counter == 0){
                                super.create(rd);
                            }                            
                        }                        
                    }else{
                        for (Recordhebahandetails rd : lrd) {
                            super.create(rd);
                        }                        
                    }
                    //end scraping                    
                } //child node
            } //node
        } //page
    }
    
    //Data scraping from Twitter --> Kuliah Maghrib di negeri Selangor
    @POST
    @Path("/dskuliahmaghribslngr")
    public void dskuliahmaghribslngr() throws IOException, TwitterException {        
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(System.getenv("TWITTER_OAUTH_CONSUMER_KEY"))
                .setOAuthConsumerSecret(System.getenv("TWITTER_OAUTH_CONSUMER_KEY_SECRET"))
                .setOAuthAccessToken(System.getenv("TWITTER_OAUTH_ACCESS_TOKEN"))
                .setOAuthAccessTokenSecret(System.getenv("TWITTER_OAUTH_ACCESS_TOKEN_SECRET"));
        List<Recordhebahandetails> hebahans = new ArrayList<>();
        TwitterFactory tf = new TwitterFactory(cb.build());
        Twitter twitter = tf.getInstance();
        Query query = new Query("'kuliah maghrib' geocode:3.059685,101.592733,100km -filter:retweets");
        QueryResult result;
        do {
            result = twitter.search(query);
            List<Status> tweets = result.getTweets();            
            for (Status tweet : tweets) {
                Recordhebahandetails hebahan = new Recordhebahandetails();
                hebahan.setUrl("https://twitter.com/search?q=kuliah+maghrib+selangor");
                hebahan.setTitle(tweet.getText());
                hebahan.setAnjuran(tweet.getText());
                hebahan.setTarikh((tweet.getCreatedAt()).toString());
                hebahan.setLokasi(tweet.getText());
                hebahan.setDetails(tweet.getText());
                hebahans.add(hebahan);
            }
        } while ((query = result.nextQuery()) != null);
        List<Recordhebahandetails> exsthebahans = super.findAll();
        if(exsthebahans.size() > 0) {
            for(Recordhebahandetails hebahan : hebahans) {
                int counter = 0;                            
                for(Recordhebahandetails exsthebahan : exsthebahans) {
                    if((exsthebahan.getTarikh()).equals(hebahan.getTarikh())) counter++;                                                                
                }
                if(counter == 0) super.create(hebahan);
            }                        
        } else {
            for (Recordhebahandetails hebahan : hebahans) {
                super.create(hebahan);
            }                        
        }
    }
    
    //Data scraping from Twitter --> Kuliah Maghrib di Kuala Lumpur
    @POST
    @Path("/dskuliahmaghribkl")
    public void dskuliahmaghribkl() throws IOException, TwitterException {        
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(System.getenv("TWITTER_OAUTH_CONSUMER_KEY"))
                .setOAuthConsumerSecret(System.getenv("TWITTER_OAUTH_CONSUMER_KEY_SECRET"))
                .setOAuthAccessToken(System.getenv("TWITTER_OAUTH_ACCESS_TOKEN"))
                .setOAuthAccessTokenSecret(System.getenv("TWITTER_OAUTH_ACCESS_TOKEN_SECRET"));
        List<Recordhebahandetails> hebahans = new ArrayList<>();
        TwitterFactory tf = new TwitterFactory(cb.build());
        Twitter twitter = tf.getInstance();
        Query query = new Query("'kuliah maghrib' geocode:3.1371102,101.68893219999995,100km -filter:retweets");
        QueryResult result;
        do {
            result = twitter.search(query);
            List<Status> tweets = result.getTweets();            
            for (Status tweet : tweets) {
                Recordhebahandetails hebahan = new Recordhebahandetails();
                hebahan.setUrl("https://twitter.com/search?q=kuliah+maghrib+kuala+lumpur");
                hebahan.setTitle(tweet.getText());
                hebahan.setAnjuran(tweet.getText());
                hebahan.setTarikh((tweet.getCreatedAt()).toString());
                hebahan.setLokasi(tweet.getText());
                hebahan.setDetails(tweet.getText());
                hebahans.add(hebahan);
            }
        } while ((query = result.nextQuery()) != null);
        List<Recordhebahandetails> exsthebahans = super.findAll();
        if(exsthebahans.size() > 0) {
            for(Recordhebahandetails hebahan : hebahans) {
                int counter = 0;                            
                for(Recordhebahandetails exsthebahan : exsthebahans) {
                    if((exsthebahan.getTarikh()).equals(hebahan.getTarikh())) counter++;                                                                
                }
                if(counter == 0) super.create(hebahan);
            }                        
        } else {
            for (Recordhebahandetails hebahan : hebahans) {
                super.create(hebahan);
            }                        
        }
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
