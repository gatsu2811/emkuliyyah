/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import entity.Hebahan;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 *
 * @author emafazillah
 */
public class UtilityTwitter {
    
    public static List<Hebahan> twitterdakwah365() throws IOException, TwitterException {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(System.getenv("TWITTER_OAUTH_CONSUMER_KEY"))
                .setOAuthConsumerSecret(System.getenv("TWITTER_OAUTH_CONSUMER_KEY_SECRET"))
                .setOAuthAccessToken(System.getenv("TWITTER_OAUTH_ACCESS_TOKEN"))
                .setOAuthAccessTokenSecret(System.getenv("TWITTER_OAUTH_ACCESS_TOKEN_SECRET"));
        List<Hebahan> hebahans = new ArrayList<>();
        TwitterFactory tf = new TwitterFactory(cb.build());
        Twitter twitter = tf.getInstance();
        Query query = new Query("source:kuliahmaghrib Dakwah365");
        QueryResult result;
        do {
            result = twitter.search(query);
            List<Status> tweets = result.getTweets();            
            for (Status tweet : tweets) {
                Hebahan hebahan = new Hebahan();
                hebahan.setUrl("https://twitter.com/Dakwah365");
                hebahan.setTitle(tweet.getText());
                hebahan.setAnjuran(tweet.getText());
                hebahan.setTarikh(tweet.getText());
                hebahan.setLokasi(tweet.getText());
                hebahan.setDetails(tweet.getText());
                hebahan.setCreateddate((tweet.getCreatedAt()).toString());
                hebahans.add(hebahan);
            }
        } while ((query = result.nextQuery()) != null);
        return hebahans;
    }
    
    public static List<Hebahan> twitterhazizulg() throws IOException, TwitterException {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(System.getenv("TWITTER_OAUTH_CONSUMER_KEY"))
                .setOAuthConsumerSecret(System.getenv("TWITTER_OAUTH_CONSUMER_KEY_SECRET"))
                .setOAuthAccessToken(System.getenv("TWITTER_OAUTH_ACCESS_TOKEN"))
                .setOAuthAccessTokenSecret(System.getenv("TWITTER_OAUTH_ACCESS_TOKEN_SECRET"));
        List<Hebahan> hebahans = new ArrayList<>();
        TwitterFactory tf = new TwitterFactory(cb.build());
        Twitter twitter = tf.getInstance();
        Query query = new Query("Kuliah Maghrib Masjid An Nur Glenmarie");
        QueryResult result;
        do {
            result = twitter.search(query);
            List<Status> tweets = result.getTweets();            
            for (Status tweet : tweets) {
                Hebahan hebahan = new Hebahan();
                hebahan.setUrl("https://twitter.com/hazizulG");
                hebahan.setTitle("Kuliah Maghrib Masjid An Nur Glenmarie");
                hebahan.setAnjuran("Masjid An Nur Glenmarie");
                hebahan.setTarikh((tweet.getCreatedAt()).toString());
                hebahan.setLokasi("Masjid An Nur Glenmarie ");
                hebahan.setDetails(tweet.getText());
                hebahan.setCreateddate((tweet.getCreatedAt()).toString());
                hebahans.add(hebahan);
            }
        } while ((query = result.nextQuery()) != null);
        return hebahans;
    }
    
    public static List<Hebahan> twittertwtlarkin() throws IOException, TwitterException {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(System.getenv("TWITTER_OAUTH_CONSUMER_KEY"))
                .setOAuthConsumerSecret(System.getenv("TWITTER_OAUTH_CONSUMER_KEY_SECRET"))
                .setOAuthAccessToken(System.getenv("TWITTER_OAUTH_ACCESS_TOKEN"))
                .setOAuthAccessTokenSecret(System.getenv("TWITTER_OAUTH_ACCESS_TOKEN_SECRET"));
        List<Hebahan> hebahans = new ArrayList<>();
        TwitterFactory tf = new TwitterFactory(cb.build());
        Twitter twitter = tf.getInstance();
        Query query = new Query("source:majlisilmu Twt_Larkin");
        QueryResult result;
        do {
            result = twitter.search(query);
            List<Status> tweets = result.getTweets();            
            for (Status tweet : tweets) {
                Hebahan hebahan = new Hebahan();
                hebahan.setUrl("https://twitter.com/Twt_Larkin");
                hebahan.setTitle(tweet.getText());
                hebahan.setAnjuran(tweet.getText());
                hebahan.setTarikh(tweet.getText());
                hebahan.setLokasi(tweet.getText());
                hebahan.setDetails(tweet.getText());
                hebahan.setCreateddate((tweet.getCreatedAt()).toString());
                hebahans.add(hebahan);
            }
        } while ((query = result.nextQuery()) != null);
        return hebahans;
    }
    
}
