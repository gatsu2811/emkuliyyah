/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import entity.Hebahan;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

/**
 *
 * @author emafazillah
 */
public class UtilityPage {
    
    public static List<Hebahan> ikimfm() throws IOException {
        List<Hebahan> hebahans = new ArrayList<>();    
        Document doc = Jsoup.connect("http://ikimfm.my/hebahanv2/index.php?").get();
        //paging
        Elements pages = doc.select("div.pagination a");
        for (Element page : pages) {
            for (Node child : page.childNodes()) {
                if (child instanceof TextNode) {                    
                    //start scraping
                    String pageno = ((TextNode) child).text();
                    String pageurl = "http://ikimfm.my/hebahanv2/index.php?page=" + pageno;
                    Document docpage = Jsoup.connect(pageurl).get();                    
                    //get all links and recursively call the processPage method
                    Elements questions = docpage.select("a[href]");
                    for (Element link : questions) {
                        if (link.attr("href").contains("hebahan-view.php?hebahanID=")) {
                            Hebahan hebahan = new Hebahan();
                            hebahan.setUrl(link.attr("abs:href"));
                            hebahan.setTitle(link.select("strong").get(0).toString());
                            hebahans.add(hebahan);
                        }
                    }
                    int row;
                    row = 0;
                    Elements eanjurans = docpage.select("div.panel-body strong");
                    for (Element eanjuran : eanjurans) {
                        if (row < hebahans.size()) {
                            for (Node childpage : eanjuran.childNodes()) {
                                if (childpage instanceof TextNode) {
                                    Hebahan hebahan = new Hebahan();
                                    String field = ((TextNode) childpage).text();
                                    switch (field) {
                                        case "Anjuran : ":
                                            hebahan.setAnjuran(eanjuran.nextSibling().toString());
                                            hebahans.get(row).setAnjuran(hebahan.getAnjuran());
                                            break;
                                        case "Tarikh : ":
                                            hebahan.setTarikh(eanjuran.nextSibling().toString());
                                            hebahans.get(row).setTarikh(hebahan.getTarikh());
                                            break;
                                        case "Lokasi : ":
                                            hebahan.setLokasi(eanjuran.nextSibling().toString());
                                            hebahans.get(row).setLokasi(hebahan.getLokasi());
                                            row++;
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    //details
                    row = 0;
                    Elements edetails = docpage.select("p.bg-info");
                    for (Element edetail : edetails) {
                        if (row < hebahans.size()) {
                            for (Node childpage : edetail.childNodes()) {
                                if (childpage instanceof TextNode) {
                                    Hebahan hebahan = new Hebahan();
                                    hebahan.setDetails(((TextNode) childpage).text());
                                    hebahans.get(row).setDetails(hebahan.getDetails());
                                    row++;
                                }
                            }
                        }
                    }
                } //child node
            } //node
        } //page        
        for(Hebahan hebahan : hebahans) {
            LocalDateTime now = LocalDateTime.now();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            hebahan.setCreateddate(dtf.format(now));
        }
        return hebahans;
    }
    
}
