/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entity.Hebahan;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import twitter4j.TwitterException;
import utility.UtilityPage;
import utility.UtilityTwitter;

/**
 *
 * @author emafazillah
 */
@Stateless
@Path("/hebahan")
public class HebahanFacadeREST extends AbstractFacade<Hebahan> {

    @PersistenceContext(unitName = "emkuliyyah_emkuliyyah_war_1.0PU")
    private EntityManager em;

    public HebahanFacadeREST() {
        super(Hebahan.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_JSON})
    public void create(Hebahan entity) {
        super.create(entity);
    }

    @PUT
    @Path("/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Hebahan entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("/{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Hebahan find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_JSON})
    public List<Hebahan> findAll() {
        return super.findAll();
    }

    @GET
    @Path("/{from}/{to}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Hebahan> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("/count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    // Truncate table
    @DELETE
    @Path("/truncatetable")
    public void cleartable() throws IOException {
        // Clear table
        List<Hebahan> hebahans = super.findAll();
        for(Hebahan hebahan : hebahans) {
            super.remove(hebahan);
        }
    }
    
    // Data scraping
    @POST
    @Path("/datascrap/{utility}")
    public void processScrap(@PathParam("utility") String utility) throws IOException, TwitterException {
        List<Hebahan> hebahans = new ArrayList<>();
        switch (utility) {
            case "ikimfm":
                hebahans = UtilityPage.ikimfm();
                break;
            case "twitterdakwah365":
                hebahans = UtilityTwitter.twitterdakwah365();
                break;
            case "twitterhazizulg":
                hebahans = UtilityTwitter.twitterhazizulg();
                break;
            case "twittertwtlarkin":
                hebahans = UtilityTwitter.twittertwtlarkin();
                break;
        }
        //check existing and insert into db
        checkExisting(hebahans, utility);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public void checkExisting(List<Hebahan> hebahans, String utility) {
        List<Hebahan> exsthebahans = super.findAll();
        if(exsthebahans.size() > 0) {
            for(Hebahan hebahan : hebahans) {
                int counter = 0;                            
                for(Hebahan exsthebahan : exsthebahans) {
                    if("ikimfm".equals(utility)) 
                        if(hebahan.getUrl().contains(exsthebahan.getUrl())) counter++;
                    else 
                        if((exsthebahan.getCreateddate()).equals(hebahan.getCreateddate())) counter++;                                        
                }
                if(counter == 0) super.create(hebahan);
            }                        
        } else {
            for (Hebahan hebahan : hebahans) {
                super.create(hebahan);
            }                        
        }
    }
    
}
