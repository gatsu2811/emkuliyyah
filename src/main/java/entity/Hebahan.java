/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author emafazillah
 */
@Entity
@Table(name = "hebahan")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hebahan.findAll", query = "SELECT h FROM Hebahan h"),
    @NamedQuery(name = "Hebahan.findById", query = "SELECT h FROM Hebahan h WHERE h.id = :id"),
    @NamedQuery(name = "Hebahan.findByUrl", query = "SELECT h FROM Hebahan h WHERE h.url = :url"),
    @NamedQuery(name = "Hebahan.findByTitle", query = "SELECT h FROM Hebahan h WHERE h.title = :title"),
    @NamedQuery(name = "Hebahan.findByAnjuran", query = "SELECT h FROM Hebahan h WHERE h.anjuran = :anjuran"),
    @NamedQuery(name = "Hebahan.findByTarikh", query = "SELECT h FROM Hebahan h WHERE h.tarikh = :tarikh"),
    @NamedQuery(name = "Hebahan.findByLokasi", query = "SELECT h FROM Hebahan h WHERE h.lokasi = :lokasi"),
    @NamedQuery(name = "Hebahan.findByDetails", query = "SELECT h FROM Hebahan h WHERE h.details = :details"),
    @NamedQuery(name = "Hebahan.findByCreateddate", query = "SELECT h FROM Hebahan h WHERE h.createddate = :createddate")})
public class Hebahan implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "url")
    private String url;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "anjuran")
    private String anjuran;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "tarikh")
    private String tarikh;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "lokasi")
    private String lokasi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "details")
    private String details;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "createddate")
    private String createddate;

    public Hebahan() {
    }

    public Hebahan(Integer id) {
        this.id = id;
    }

    public Hebahan(Integer id, String url, String title, String anjuran, String tarikh, String lokasi, String details, String createddate) {
        this.id = id;
        this.url = url;
        this.title = title;
        this.anjuran = anjuran;
        this.tarikh = tarikh;
        this.lokasi = lokasi;
        this.details = details;
        this.createddate = createddate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAnjuran() {
        return anjuran;
    }

    public void setAnjuran(String anjuran) {
        this.anjuran = anjuran;
    }

    public String getTarikh() {
        return tarikh;
    }

    public void setTarikh(String tarikh) {
        this.tarikh = tarikh;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hebahan)) {
            return false;
        }
        Hebahan other = (Hebahan) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Hebahan[ id=" + id + " ]";
    }
    
}
